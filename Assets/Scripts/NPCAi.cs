﻿using System;
using System.Collections;
using UnityEngine;

public class NPCAi : MonoBehaviour
{
   public GameObject playerGameobject;
   public Animator animator;

   public int aiID;
   public Transform handTrans;
   
   public GameObject[] waypointList;
   
   
   [Range(0f,5f)]
   public float triggerStayTime;

   private float m_PassedTime;
   private bool m_Isgrabbing;
   
   private void Awake()
   {
      playerGameobject = GameObject.FindGameObjectWithTag("Player");
      animator = transform.GetChild(0).GetComponent<Animator>();
   }

   private void Update()
   {
      float t_Distance = Vector3.Distance(transform.position, playerGameobject.transform.position);
      
      animator.SetFloat("distance",t_Distance);
   }

   private void OnCollisionStay(Collision other)
   {
      if (other.transform.CompareTag("Player"))
      {
         m_PassedTime += Time.deltaTime;
         //Debug.Log(m_PassedTime);
         if (m_PassedTime >= triggerStayTime)
         {
            m_PassedTime = 0f;
            StartCoroutine(GrabRoutine());
         }
      }
   }

   IEnumerator GrabRoutine()
   {
      Debug.LogError("CAUGHT  " +aiID);
      animator.SetBool("grab",true);
      m_Isgrabbing = true;
      yield return new WaitForEndOfFrame();
      transform.position = new Vector3(transform.position.x,transform.position.y -.5f,transform.position.z);
     // yield return new WaitForSeconds(.2f);
     // PlayerController.Instance.SetPlayerPos(handTrans.position);
      UIManager.Instance.ShowLevelFailed();
      
      yield return new WaitForSeconds(1f);
      Gameplay.Instance.LevelFailed();
   }

   public void DisableAnimator()
   {
      animator.enabled = false;
   }

   public void Freeze()
   {
      GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
      GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
   }

   private void OnTriggerExit(Collider other)
   {
      m_PassedTime = 0;
   }

   private void OnCollisionExit(Collision other)
   {
      if (other.transform.CompareTag("Player"))
      {
         m_PassedTime = 0;
      }
   }

   public void StartMove()
   {
      animator.SetTrigger("start");
   }

   public void SetDuckWithHand()
   {
      Debug.LogError("Its Calling");
      if (m_Isgrabbing)
      {
         PlayerController.Instance.GetPlayerTrans().SetParent(handTrans);
         PlayerController.Instance.SetPlayerLocalPos(Vector3.zero, handTrans);
      }
   }
}
