﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObjectiveController : MonoBehaviour
{
   public static ObjectiveController Instance;
   
   public GameObject starPrefab;
   public List<Transform> starInsTransList;

   private bool m_IsStartInstance;
   
   private float m_PassedTime;
   private int m_NextTime;
   
   private void Awake()
   {
      if (Instance == null)
      {
         Instance = this;
      }
   }

   private void Start()
   {
      m_NextTime = Random.Range(10, 20);
   }

   private void Update()
   {
      if (Gameplay.Instance.isGameStart)
      {
         m_PassedTime += Time.deltaTime;
         if (!m_IsStartInstance)
         {
            if (m_PassedTime > m_NextTime)
            {
               m_IsStartInstance = true;
               m_PassedTime = 0;
               InstantiateStar();
            }
         }
      }
   }


   public void InstantiateStar()
   {
      m_NextTime = Random.Range(15, 25);
      
      int t_RandValue = Random.Range(0, starInsTransList.Count);
      
      GameObject t_Star = Instantiate(starPrefab,starInsTransList[t_RandValue].position, Quaternion.identity,transform);
      m_IsStartInstance = false;
   }
}
