﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarRotate : MonoBehaviour
{
    public ParticleSystem burstParticle;
    public MeshRenderer starMesh;


    private void Update()
    {
        if (IsVisible())
        {
            UIManager.Instance.DeactivateRadarImage();
        }
        else
        {
            Gameplay.Instance.ShowStarPos(transform);
        }
    }

    private bool IsVisible() 
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        if (GeometryUtility.TestPlanesAABB(planes , GetComponent<Collider>().bounds))
            return true;
        else
            return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.LogError("Player Colldierd");
            StartCoroutine(CollideRoutine());
        }
    }

    IEnumerator CollideRoutine()
    {
        starMesh.enabled = false;
        burstParticle.Play();
        yield return new WaitForSeconds(1f);
        Gameplay.Instance.StarCollided();
        Destroy(this.gameObject);
    }
}


