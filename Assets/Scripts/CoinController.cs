﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CoinController : MonoBehaviour
{
    public static CoinController Instance;

    public GameObject coinPrefab;
    public List<Transform> coinInsTransList;

    private bool m_IsStartInstance;
   
    private float m_PassedTime;
    private int m_NextTime;
   
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        //m_NextTime = Random.Range(1, 3);
    }

    private void Update()
    {
        // m_PassedTime += Time.deltaTime;
        // if (!m_IsStartInstance)
        // {
        //     if (m_PassedTime > m_NextTime)
        //     {
        //         m_IsStartInstance = true;
        //         m_PassedTime = 0;
        //         InstantiateCoin();
        //     }
        // }
    }


    public void InstantiateCoin()
    {
      ///  m_NextTime = Random.Range(1, 3);
      
        int t_RandValue = Random.Range(0, coinInsTransList.Count);
      
        GameObject t_Star = Instantiate(coinPrefab,coinInsTransList[t_RandValue].position, Quaternion.identity,transform);
        m_IsStartInstance = false;
    }
}
