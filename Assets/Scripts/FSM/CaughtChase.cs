﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaughtChase : NPCBaseFSM
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);
        
        Vector3 t_Direction =PlayerController.Instance.GetPlayerTrans().position - nPCCharacter.transform.position; 
        //nPCCharacter.transform.rotation = Quaternion.Slerp(nPCCharacter.transform.rotation,Quaternion.LookRotation(t_Direction),rotSpeed*Time.deltaTime );
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        nPCCharacter.transform.position = new Vector3(nPCCharacter.transform.position.x,nPCCharacter.transform.position.y+01f,nPCCharacter.transform.position.z);
    }
}
