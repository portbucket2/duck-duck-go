﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowRotate : MonoBehaviour {

    [SerializeField]
    private Transform target;

    public Transform playerTrans;
    
    [SerializeField]
    private Vector3 offsetPosition;

    [SerializeField]
    private Space offsetPositionSpace = Space.Self;

    [SerializeField]
    private bool lookAt = true;
    public float smoothSpeed = 0.15f;
    private void FixedUpdate()
    {
        Refresh();
    }

    public void Refresh()
    {
        if (target == null)
        {
            Debug.LogWarning("Missing target ref !", this);

            return;
        }

        // compute position
        // if (offsetPositionSpace == Space.Self)
        // {
        //     transform.position = target.TransformPoint(offsetPosition);
        // }
        // else
        // {
        //     transform.position = target.position + offsetPosition;
        // }

        Vector3 desiredPosition = playerTrans.TransformPoint(offsetPosition); //playerTrans.position + offsetPosition;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;
        
        

        // compute rotation
        if (lookAt)
        {
            transform.LookAt(target);
            //transform.rotation = Quaternion.Lerp(transform.rotation,playerTrans.rotation,Time.deltaTime * 5);
        }
        else
        {
            Debug.Log("Calleing");
            //transform.rotation = target.rotation;
            transform.rotation = Quaternion.Lerp(transform.rotation,playerTrans.rotation,Time.deltaTime * 5);
        }
    }
}

