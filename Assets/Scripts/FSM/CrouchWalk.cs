﻿using UnityEngine;

public class CrouchWalk : NPCBaseFSM
{
    private GameObject[] wayPoints;
    private int currentWP;

    private void Awake()
    {
        
        //GameObject.FindGameObjectsWithTag("waypoint");
    }
    

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        wayPoints = nPCCharacter.GetComponent<NPCAi>().waypointList;
        currentWP = 0;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(wayPoints.Length ==0)
            return;

        if (Vector3.Distance(wayPoints[currentWP].transform.position, nPCCharacter.transform.position) < accuracy)
        {
            currentWP++;
            if (currentWP >= wayPoints.Length)
            {
                currentWP = 0;
            }
        }
        
        

        Vector3 t_Direction = wayPoints[currentWP].transform.position - nPCCharacter.transform.position;
        if (nPCCharacter.transform.position.y > 0)
        {
            nPCCharacter.transform.position = new Vector3(nPCCharacter.transform.position.x,0,nPCCharacter.transform.position.z);
        }
        
        nPCCharacter.transform.rotation = Quaternion.Slerp(nPCCharacter.transform.rotation,Quaternion.LookRotation(t_Direction),rotSpeed*Time.deltaTime );
        nPCCharacter.transform.Translate(0,0,Time.deltaTime *movmentSpeed);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }
}
