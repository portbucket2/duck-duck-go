﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCChase : NPCBaseFSM
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var t_Direction = playerGameobject.transform.position - nPCCharacter.transform.position;
        
        nPCCharacter.transform.rotation = Quaternion.Slerp(
            nPCCharacter.transform.rotation,
            Quaternion.LookRotation(t_Direction),
            rotSpeed * Time.deltaTime
            );
        
        nPCCharacter.transform.Translate(0,0,Time.deltaTime * chaseSpeed);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        nPCCharacter.GetComponent<NPCAi>().SetDuckWithHand();
    }
}
