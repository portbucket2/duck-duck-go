﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelManager))]
public class LevelManagerEditor : Editor
{
   private LevelManager m_LevelManager;

   private void OnEnable()
   {
      m_LevelManager = (LevelManager) target;
   }

   public override void OnInspectorGUI()
   {
      base.OnInspectorGUI();
      
      GUILayout.BeginHorizontal();
      GUILayout.Label("Current Level : "+m_LevelManager.GetCurrentLevel());

      if (GUILayout.Button("Increase"))
      {
         m_LevelManager.IncreaseGameLevel();
      }
      if (GUILayout.Button("Decrease"))
      {
         m_LevelManager.DecreaseGameLevel();
      }

      if (GUILayout.Button("Reset Level"))
      {
         m_LevelManager.ResetLevel();
      }
      GUILayout.EndHorizontal();
   }
}
