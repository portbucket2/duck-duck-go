﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    // private void OnTriggerEnter(Collider other)
    // {
    //    
    //     if (other.CompareTag("Player"))
    //     {
    //         PlayerController.Instance.TouchedWall(true);
    //     }
    // }
    //
    // private void OnTriggerExit(Collider other)
    // {
    //     if (other.CompareTag("Player"))
    //     {
    //         PlayerController.Instance.TouchedWall(false);
    //     }
    // }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Player"))
        {
            other.transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}
