﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBaseFSM : StateMachineBehaviour
{
    protected GameObject nPCCharacter;
    protected GameObject playerGameobject;

    protected float movmentSpeed = 2f;
    protected float chaseSpeed = 3.5f;
    protected float rotSpeed = 2.0f;
    protected float accuracy = 1.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        nPCCharacter = animator.transform.parent.gameObject;
        playerGameobject = GameObject.FindGameObjectWithTag("Player");
    }
}
