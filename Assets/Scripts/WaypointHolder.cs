﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointHolder : MonoBehaviour
{
    public static WaypointHolder Instance;

    public WaypointID[] waypointList;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public GameObject[] GetTrasformArrayBasedonId(int t_ID)
    {
        return waypointList[t_ID].waypointList;
    }
}


[System.Serializable]
public struct WaypointID
{
    public int id;
    public GameObject[] waypointList;
}