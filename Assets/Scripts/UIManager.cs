﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.SDK;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Start Panel")] public TextMeshProUGUI levelNumberText;
    public Button tapButton;
    public Animator startPanelAnimator;

    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private static UIManager m_Instance;

    [Header("Upper panel")] public Animator upperPanelAnimator;
    public TextMeshProUGUI currentLevelText;
    public TextMeshProUGUI currentCoinText;

    [Header("Level Complete")] public Animator levelCompleteAnimator;
    public Button collectButton;
    public ParticleSystem levelCompleteParticle;
    public Image levelUpBack;

    public GameObject dynamicGameobject;

    [Header("Level Failed")] public Animator levelFailedAnimator;
    public Button tryAgainButton;
    public Image levelFailedBack;
    
    private bool m_IsLevelFailed;

    [Header("Star")] public List<GameObject> fillStarList;
    [Header("Objective")] public Animator objectiveAnimator;

    public RectTransform raderImage;
    public Image dollarImage;
    public Image starImage;

    public static UIManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    private void Start()
    {
        currentLevelText.text = LevelManager.Instance.GetCurrentLevelWithLevelText();
        levelNumberText.text = LevelManager.Instance.GetCurrentLevelWithLevelText();
        ButtonInteraction();
    }


    private void ButtonInteraction()
    {
        tapButton.onClick.AddListener(delegate
        {
            startPanelAnimator.SetTrigger(EXIT);
            upperPanelAnimator.SetTrigger(ENTRY);
            m_IsLevelFailed = false;
            
            FacebookAnalyticsManager.Instance.FBALevelStart(LevelManager.Instance.GetCurrentLevel());
            
            Gameplay.Instance.StartGame();
        });
    }

    public void UpdateCurrency()
    {
        currentCoinText.text = Gameplay.Instance.GetCurrentCurrency().ToString();
    }

    public void ActivateDynamicJoystick()
    {
        dynamicGameobject.SetActive(true);
    }

    public void DeactivateDynamicJoystick()
    {
        dynamicGameobject.SetActive(false);
    }

    public void ShowLevelComplete()
    {
        StartCoroutine(LevelCompleteRoutine());
    }

    IEnumerator LevelCompleteRoutine()
    {
        
        FacebookAnalyticsManager.Instance.FBALevelComplete(LevelManager.Instance.GetCurrentLevel());
        
        Gameplay.Instance.LevelFailed();
        
        yield return new WaitForSeconds(0.25f);
        levelUpBack.enabled = true;
        levelCompleteAnimator.SetTrigger(ENTRY);
        DeactivateDynamicJoystick();
        
        levelCompleteParticle.Play();
        
        LevelManager.Instance.IncreaseGameLevel();

        collectButton.onClick.AddListener(delegate
        {
            SceneManager.LoadScene("Duck-Go-Gameplay-Goose-Cam");
        });
    }

    public void ShowLevelFailed()
    {
        StartCoroutine(LevelFailedRoutine());
    }

    IEnumerator LevelFailedRoutine()
    {
        FacebookAnalyticsManager.Instance.FBALevelFailed(LevelManager.Instance.GetCurrentLevel());
        yield return new WaitForSeconds(1f);
        if (!m_IsLevelFailed)
        {
            DeactivateDynamicJoystick();
            m_IsLevelFailed = true;
            levelFailedBack.enabled = true;
            levelFailedAnimator.SetTrigger(ENTRY);
            tryAgainButton.onClick.AddListener(delegate
            {
                SceneManager.LoadScene("Duck-Go-Gameplay-Goose-Cam");
            });
        }
    }

    public void AppearStar(int t_Index)
    {
        fillStarList[t_Index].SetActive(true);
    }

    public void ShowObjectivePanel()
    {
        objectiveAnimator.SetTrigger(ENTRY);
    }

    public void DisappearObjectivePanel()
    {
        objectiveAnimator.SetTrigger(EXIT);
    }

    public void SetRadarImagePos(Vector3 t_Pos)
    {
        raderImage.position = t_Pos;
    }

    public void ActivateRadarImage()
    {
        raderImage.gameObject.SetActive(true);
    }

    public void DeactivateRadarImage()
    {
        raderImage.gameObject.SetActive(false);
    }

    public void ActivateDollarImage()
    {
        dollarImage.gameObject.SetActive(true);
        starImage.gameObject.SetActive(false);
    }

    public void ActivateStarImage()
    {
        dollarImage.gameObject.SetActive(false);
        starImage.gameObject.SetActive(true);
    }
}
