﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBaseSlowWalk : StateMachineBehaviour
{
    protected GameObject nPCCharacter;
    protected GameObject playerGameobject;

    protected float movmentSpeed = .5f;
    protected float chaseSpeed = 2f;
    protected float rotSpeed = 2.0f;
    protected float accuracy = 1.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        nPCCharacter = animator.transform.parent.gameObject;
        playerGameobject = GameObject.FindGameObjectWithTag("Player");
    }
}
