﻿
using UnityEngine;
using Random = UnityEngine.Random;

// public class DefaultAudience : MonoBehaviour
// {
//     private void Start()
//     {
//         int t_RanValue = Random.Range(0, 5);
//         
//         transform.GetChild(0).GetComponent<Animator>().SetInteger("value",t_RanValue);
//     }
// }


public class DefaultAudience : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        int t_RanValue = Random.Range(0, 5);
        
        animator.SetInteger("value",t_RanValue);
    }
}