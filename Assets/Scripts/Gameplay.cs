﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.SDK;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;
    
    public Transform cubeTrans;
    [Range(0f, 10f)] public float movementSpeed;
    
    private int m_StarCounter;

    public List<NPCAi> npcPlayerList;

    public Material envMat;
    public List<Texture> envTextureList;

    public CameraFollowRotate camerafollowRoate;
    
    private int m_TotalCurrency;

    public bool isGameStart;
    
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        m_TotalCurrency = 0;
    }

    private void Start()
    {
        camerafollowRoate.enabled = true;
        ChangeEnv();
    }

    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
    }

    IEnumerator StartGameRoutine()
    {
        isGameStart = true;
        
        yield return new WaitForSeconds(0.2f);
        UIManager.Instance.ShowObjectivePanel();
        yield return new WaitForSeconds(1.5f);
        UIManager.Instance.DisappearObjectivePanel();
        
        //CameraMovementController.Instance.FocusCameraAsArea(CameraMovementController.Instance.cameraTargetInEditor);
        //CameraMovementController.Instance.FocusCameraWithOrigin(CameraMovementController.Instance.cameraOriginInEditor,CameraMovementController.Instance.cameraTargetInEditor);
       
        yield return new WaitForSeconds(0.1f);
        UIManager.Instance.ActivateDynamicJoystick();
        StartNPCMovement();
        
        yield return new WaitForSeconds(1f);
        CoinController.Instance.InstantiateCoin();
    }

    public void ChangeEnv()
    {
        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();
        Debug.Log(t_CurrentLevel);

        if (t_CurrentLevel > 2)
        {
            envMat.mainTexture = envTextureList[1];
        }
        else
        {
            envMat.mainTexture = envTextureList[t_CurrentLevel - 1];
        }
    }

    public void StarCollided()
    {
     UIManager.Instance.AppearStar(m_StarCounter);
     m_StarCounter++;

     if (m_StarCounter == 3)
     {
         UIManager.Instance.ShowLevelComplete();
     }
    }

    public void StartNPCMovement()
    {
        for (int i = 0; i < npcPlayerList.Count; i++)
        {
            npcPlayerList[i].StartMove();
        }
    }

    public void IncreaseCurrency()
    {
        m_TotalCurrency++;
        
        UIManager.Instance.UpdateCurrency();
        
        CoinController.Instance.InstantiateCoin();
    }

    public int GetCurrentCurrency()
    {
        return m_TotalCurrency;
    }


    public void ShowStarPos(Transform t_StarTrans)
    {
        Vector3 t_Dir = PlayerController.Instance.GetPlayerTrans().position - t_StarTrans.position;

        RaycastHit hit;
        if (Physics.Raycast(PlayerController.Instance.GetPlayerTrans().position, -t_Dir, out hit))
        {
           // Debug.DrawRay(PlayerController.Instance.GetPlayerTrans().position, -t_Dir * hit.distance, Color.blue);
            if (hit.transform.CompareTag("Bar"))
            {
                cubeTrans.position = Vector3.Lerp(cubeTrans.position, hit.point, Time.deltaTime * movementSpeed);
                Vector3 t_Pos = Camera.main.WorldToScreenPoint(hit.point);
                UIManager.Instance.ActivateDollarImage();
                UIManager.Instance.ActivateRadarImage();
                UIManager.Instance.SetRadarImagePos(t_Pos);
            }
        }
    }

    public void LevelFailed()
    {
        isGameStart = false;
        camerafollowRoate.enabled = false;
        for (int i = 0; i < npcPlayerList.Count; i++)
        {
            npcPlayerList[i].DisableAnimator();
            npcPlayerList[i].Freeze();
        }
    }
}
